import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'all-pages', pathMatch: 'full' },
  { path: 'logo', loadChildren: './pages/logo/logo.module#LogoPageModule' },
  { path: 'registration-number', loadChildren: './pages/registration-number/registration-number.module#RegistrationNumberPageModule' },
  { path: 'registration-number/:form', loadChildren: './pages/registration-number/registration-number.module#RegistrationNumberPageModule' },
  { path: 'task-client', loadChildren: './pages/task-client/task-client.module#TaskClientPageModule' },
  { path: 'task-executor', loadChildren: './pages/task-executor/task-executor.module#TaskExecutorPageModule' },
  { path: 'all-pages', loadChildren: './pages/all-pages/all-pages.module#AllPagesPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-registration-number',
    templateUrl: './registration-number.page.html',
    styleUrls: ['./registration-number.page.scss'],
})
export class RegistrationNumberPage implements OnInit {

    id: any;
    form: any;
    name: string;
    number: any;
    name2 = 'Геннадий';
    number2 = "+7(930)320-40-30";


    constructor(public route: ActivatedRoute) {
    }

    ngOnInit() {
        if( this.id = this.route.snapshot.params['form']) {
            this.name = this.name2;
            this.number = this.number2;
        };
    }

}

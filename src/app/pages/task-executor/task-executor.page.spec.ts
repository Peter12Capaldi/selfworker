import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskExecutorPage } from './task-executor.page';

describe('TaskExecutorPage', () => {
  let component: TaskExecutorPage;
  let fixture: ComponentFixture<TaskExecutorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskExecutorPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskExecutorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

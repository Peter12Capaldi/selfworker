import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllPagesPage } from './all-pages.page';

describe('AllPagesPage', () => {
  let component: AllPagesPage;
  let fixture: ComponentFixture<AllPagesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllPagesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllPagesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskClientPage } from './task-client.page';

describe('TaskClientPage', () => {
  let component: TaskClientPage;
  let fixture: ComponentFixture<TaskClientPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskClientPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskClientPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
